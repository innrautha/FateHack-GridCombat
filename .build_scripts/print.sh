#!/bin/bash
#
# script for generating the print pdfs
# this script should be called from the Makefile in the document root
#
# check number of arguments with `$# -eq 0`
#

XELATEX_OPTS=""
BIBER_OPTS=""

if [ $# == 3 ]; then
    echo "Changing directory to: $1"
    cd $1
    if [ $3 = 'true' ]; then
        echo -e "\n------------------ xelatex --------------------\n"     
        xelatex $XELATEX_OPTS ${2}_print > /dev/null
        echo -e "\n--------------- makeglossaries ----------------\n"         
        makeglossaries ${2}_print
        echo -e "\n------------------ xelatex --------------------\n" 
        xelatex $XELATEX_OPTS ${2}_print | grep --ignore-case --extended-regex "info|warning|error|^\([A-Za-z0-9]*\)"
    else
        xelatex $XELATEX_OPTS ${2}_print > /dev/null
        makeglossaries ${2}_print > /dev/null   
        xelatex $XELATEX_OPTS ${2}_print > /dev/null
    fi
else
    echo "Number of arguments given: $#"
    echo "Usage: ./screen.sh path_to_build_dir projectname [true/false]"
fi
