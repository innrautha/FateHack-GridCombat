#!/bin/bash
#
# script for generating the print pdfs
# this script should be called from the Makefile in the document root
#
# check number of arguments with `$# -eq 0`
#

if [ $# == 3 ]; then
    echo "Changing directory to: $1"
    cd $1
    if [ $3 = 'true' ]; then
        echo "12345: $XELATEX_OPTS"
        echo -e "\n------------------ xelatex --------------------\n"     
        xelatex $XELATEX_OPTS $2 #|grep --ignore-case --extended-regex "info|warning|error|^\([A-Za-z0-9]*\)"
        echo -e "\n--------------- makeglossaries ----------------\n"         
        makeglossaries $2
        echo -e "\n------------------ xelatex --------------------\n" 
        xelatex $XELATEX_OPTS $2 > /dev/null
    else
        xelatex $XELATEX_OPTS $2 > /dev/null   
        makeglossaries $2 > /dev/null   
        xelatex $XELATEX_OPTS $2 > /dev/null
    fi
else
    echo "Number of arguments given: $#"
    echo "Usage: ./screen.sh path_to_build_dir projectname [true/false]"
fi
